<?php
require '../autoload.php';

use Multitenant\Core\Classes\CourseManager;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Classes\ClientManager;


require_once("$CFG->libdir/formslib.php");
include("{$MULTI->forms}/courseallocation_form.php");
include($CFG->dirroot.'/Multitenant/helper.php');
$clientid = optional_param('clientid', false, PARAM_INT);

$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Add Client');
$PAGE->set_heading('Add Client');
$PAGE->set_url($MULTI->web.'/course_allocation.php');
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('courseallocation'), 'side-pre');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Course Allocation');
$cm = new ClientManager();
$lists = $cm->get_clients();
$optionlist['none'] = '-- select --';
foreach ($lists as $list) {
        $optionlist[$list->get_id()] = $list->get_name();
}
echo $OUTPUT->header();
echo html_writer::start_div();
if (!empty($clientid)) {
        echo html_writer::div(sprintf('You have selected <strong>%s</strong> client', $optionlist[$clientid]), 'alert alert-success');
}
echo html_writer::tag('p', 'Select client', array('style' => 'font-weight:600'));
echo $OUTPUT->single_select(new moodle_url($CFG->wwwroot . '/Multitenant/web/course_allocation.php'), 'clientid', $optionlist, $clientid, null, 'rolesform');
echo html_writer::end_div();
$allocatecourseform = new courseallocation_form(null, array('clientid' => $clientid));
if ($allocatecourseform->is_cancelled()) {
    
} else if ($data = $allocatecourseform->get_data()) {
    if (!empty($data->clientid)) {
        $cs = new CourseManager();
        $courses = $cs->allocate_course($data);
        echo '<div class="alert alert-success">Courses have been allocated successfully to client <strong>' . $cm->get_client($data->clientid)->get_name() . '</strong></div>';
        purge_all_caches();
    } else {
        echo html_writer::div(sprintf('You have not selected  any Clients. Please select one.'), 'alert alert-error');
    }
}
$allocatecourseform->display();
echo $OUTPUT->footer();
