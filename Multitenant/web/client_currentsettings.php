<?php

require '../autoload.php';

use moodle_exception;
use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\ClientsNotFoundException;

$clientid = optional_param('id', '', PARAM_INT);
require_once($CFG->libdir . '/adminlib.php');
include($CFG->dirroot . '/Multitenant/helper.php');
require_once("$CFG->libdir/formslib.php");
include($CFG->dirroot . '/Multitenant/Forms/client_plugin_form.php');
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Client current settings');
$PAGE->set_heading('Client current settings');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/client_currentsettings.php');
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('currentsettings'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Client current settings');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery-1.12.0.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery.dataTables.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.fixedColumns.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.html5.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.flash.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.print.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.buttons.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jszip.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/pdfmake.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/vfs_fonts.js'),true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery.dataTables.min.css'));
// Not support less then bootstrap3
//$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.bootstrap.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/styles.css'),true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.dataTables.min.css'));

$cm = new ClientManager();
$alert = 'alert alert-success';
$msg = '';
try {
    $lists = $cm->get_clients();
} catch (ClientsNotFoundException $e) {
    $msg = 'Clients not found';
}
$client = null;
$optionlist = array();
$optionlist['none'] = '-- Select --';
if (!empty($lists)) {
        foreach ($lists as $list) {
        $optionlist[$list->get_id()] = $list->get_name();
    }
}
$clientname = '';
if (isset($optionlist[$clientid])) {    
    $msg = 'Client <b>%s</b> has been selected';
    $clientname = $optionlist[$clientid];
}
$currentsettings = null;
$activedata = null;
if(!empty($clientid)) {
    $client = $lists[$clientid];
    try {
        $currentsettings = $cm->get_client_config_plugins($client);
        if(is_null($currentsettings)) {
            $msg = 'Error on reading data from client database!!';
            $alert = 'alert alert-warning';
        }
    } catch(moodle_exception $e) {
        $msg = 'Error on reading data from client database !!';
        $alert = 'alert alert-error';
    }
}

echo $OUTPUT->header();
// Not support less then bootstrap3
//echo '<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">';
echo html_writer::tag('span', 'Client current settings', array('class' => 'lead'));
echo html_writer::empty_tag('hr');
echo html_writer::start_div('', array('style' => 'margin-left:18.5%;margin-bottom:8px'));
echo html_writer::tag('span', 'Select client', array('style' => 'font-weight:600;padding-right:10px'));
echo $OUTPUT->single_select($url, 'id', $optionlist, $clientid, null, 'rolesform');
echo html_writer::end_div();
if(!empty($clientname)) {
    echo html_writer::tag('p', sprintf($msg, $clientname), array('class' => $alert.' mrx'));
}
if($currentsettings) {
    $table = new html_table();
    $table->id = 'example';
    $table->attributes = array('class' => 'table table-striped table-bordered table-hover');
    $table->head = array('S.L.','Type','Modname', 'Enabled\Disabled');
    $table->size = ['10','20','40','30'];
    $types = array(
                    'assignfeedback'=> 'Assign feedback',
                    'assignsubmission'=> 'Assign submission',
                    'auth'=> 'Authentication',
                    'block'=> 'Blocks',
                    'enrol'=> 'Enrollment',
                    'mod'=> 'Activity',
                    'qtype'=> 'Question types',
                  );
    foreach($currentsettings as $type => $isenabled) {
        static $i = 1;
        if($isenabled) {
            $isenabled = 'Enable';
        } else {
            $isenabled = 'Disable';
        }
        if ($PAGE->theme->resolve_image_location('icon', $type)) {
            $icon = $OUTPUT->pix_icon('icon', '', $type, array('class' => 'icon pluginicon'));
        } else {
            $icon = $OUTPUT->pix_icon('spacer', '', 'moodle', array('class' => 'icon pluginicon noicon'));
        }
        $modname = explode('_', $type, 2);
        $table->data[] = array($i++,$types[($modname[0])], $icon.$modname[1], $isenabled);
    }
    echo html_writer::table($table);
}
echo '<script type="text/javascript">
        $(document).ready(function() {
            $("#example").DataTable({
                   dom: "Bfrtip",
                   buttons: [
                             "copy", "csv", "excel", "pdf", "print"
                    ]
                } );
           });
      </script>';
echo $OUTPUT->footer();
