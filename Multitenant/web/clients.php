<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require '../autoload.php';

use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\ClientsNotFoundException;

require_once("$CFG->libdir/formslib.php");
include("{$MULTI->forms}/client_setting_form.php");
include($CFG->dirroot . '/Multitenant/helper.php');
$clientid = optional_param('id', '', PARAM_INT);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Client list');
$PAGE->set_heading('Client list');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/Client_settings.php');
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('clients'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Client list');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery-1.12.0.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery.dataTables.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.fixedColumns.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.html5.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.flash.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.print.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.buttons.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jszip.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/pdfmake.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/vfs_fonts.js'),true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/jquery.dataTables.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/buttons.dataTables.min.css'));
// Not support bootstrap3
//$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/Multitenant/web/js/dataTables.bootstrap.min.css'));
$PAGE->requires->css('/Multitenant/web/styles.css');
echo $OUTPUT->header();
// Not support bootstrap3
//echo '<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">';
$cm = new ClientManager();
try {
    $clients = $cm->get_clients(true);
} catch (ClientsNotFoundException $e) {
    echo html_writer::tag('div', $e->getMessage(), array('class' => 'alert alert-warning'));
}
if (isset($clients)) {
    $table = new html_table();
    $table->id = "example";
    $table->attributes = array('class' => 'table table-striped table-bordered table-hover');
    $table->head = array(
        'S.L.',
        'Name',
        'Poc Name',
        'Email',
        'Phone',
        'Sub domain',
        'Status',
        'Site Url',
        'Start date',
        'End date',
        'Max Users',
        'Active Users',
        'Inactive Users',
        'Max courses',
        'Database max storage',
        'Disk max storage',
        'Limit concurrent logins',
        'Database free space',
        'Database used space',
        'Datafolder used space',
        'Action');
    foreach ($clients as $client) {
        $row = $client->get();
        static $sl = 1;
        $usage = $row['client_usage'];
        $status = '<span class="label label-warning">In-active</span>';
        if ($row['status']) {
            $status = '<span class="label label-success">Active</span>';
        }
        $maxdisk = $usage->get_max_datastorage();
        if($maxdisk == 0) {
            $maxdisk = 'Unlimited';
        }  else {
            $maxdisk .= ' GB';
        }
        $maxdb = $usage->get_max_dbstorage();
        if($maxdb == 0) {
            $maxdb = 'Unlimited';
        } else {
            $maxdb .= ' GB';
        }
        $enddate = 'Unlimited';
        if($row['end_date'] != 0) {
            $enddate = userdate($row['end_date'], '%d/%m/%y');
        }
        $table->data[] = array(
            $sl,
            $row['name'],
            $row['pocname'],
            $row['email'],
            $row['pocphone'],
            $row['sub_domain'],
            $status,
            $row['site_url'],
            userdate($row['start_date'], '%d/%m/%y'),
            $enddate,
            ($row['max_users'] == 0 ? 'Unlimited' : $row['max_users']),
            $usage->get_active_users(),
            $usage->get_inactive_users(),
            ($row['max_courses'] == 0 ? 'Unlimited' : $row['max_courses']),
            $maxdisk,
            $maxdb,
            ($row['limit_concurrentlogins'] == 0 ? 'Unlimited' : $row['limit_concurrentlogins']),
            size_format($usage->get_database_freespace()),
            size_format($usage->get_database_usedspace()),
            size_format($usage->get_disk_usedspace()),
            html_writer::link(new moodle_url($CFG->wwwroot . '/Multitenant/web/edit_client.php', array('id' => $row['id'])), 'Edit')
        );
        $sl++;
    }
    echo html_writer::div('Client list', 'lead');
    echo html_writer::empty_tag('hr');
    echo html_writer::div(html_writer::table($table));
}
echo '<script type="text/javascript">
        $(document).ready(function() {
            $("#example").DataTable( {
                scrollX:        true,
                dom: "Bfrtip",
                buttons: [
                          "copy", "csv", "excel", "pdf", "print"
               ]
            } );
        });
        </script>';
echo $OUTPUT->footer();
