<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require '../autoload.php';

require_once("$CFG->libdir/formslib.php");
require_once("$CFG->libdir/installlib.php");
include_once("{$MULTI->forms}/addclient_form.php");
include($CFG->dirroot . '/Multitenant/helper.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title('Dashboard');
$PAGE->set_heading('Dashboard');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));
$PAGE->set_url(new moodle_url($CFG->wwwroot . '/Multitenant/web/create_client.php'));
$regions = $PAGE->blocks->get_default_region();
$bc = new block_contents();
$PAGE->blocks->add_fake_block(navigation_menu('dashboard'), $regions);
$PAGE->blocks->show_only_fake_blocks();

$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Dashboard');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/style.css'));
echo $OUTPUT->header();
$btn = html_writer::link('#', '&times;', array('class' => 'close', 'data-dismiss' => 'alert', 'aria-label' => 'close'));
echo html_writer::tag('p', 'Please click to on the left menus to start' . $btn, array('class' => 'alert alert-success lead'));
echo $OUTPUT->footer();
