<?php
// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require '../autoload.php';

use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;

require_once("$CFG->libdir/formslib.php");
include("{$MULTI->forms}/addclient_form.php");
include($CFG->dirroot . '/Multitenant/helper.php');

$clientid = optional_param('id', false, PARAM_INT);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Add Client');
$PAGE->set_heading('Add Client');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/edit_client.php', array('id' => $clientid));
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('createclient'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Add client');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));
$mf = new addclient_form($url, array('id' => $clientid));
$cm = new ClientManager();
$client = $cm->get_client($clientid);
$defaultdata = $client->get(true);
$defaultdata['max_dbstorage'] = $client->get_max_dbstorage();
$defaultdata['max_datastorage'] = $client->get_max_datastorage();
$mf->set_data($defaultdata);
$msg = '';
if ($mf->is_cancelled()) {
    
} else if ($data = $mf->get_data()) {
    // ceate instance of CLientManager i
    $cm = new ClientManager();
    $client = new Client();
    $data->id = $clientid;
    $client->set($data);
    if ($cm->update_client($client)) {
        redirect($CFG->wwwroot . '/Multitenant/web/clients.php');
    }
}
echo $OUTPUT->header();
if ($mf != null) {
    $mf->display();
}
echo $OUTPUT->footer();
