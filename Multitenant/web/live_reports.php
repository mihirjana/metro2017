<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require '../autoload.php';

use Multitenant\Core\Classes\ClientManager;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\ClientsNotFoundException;

require_once("$CFG->libdir/formslib.php");
include("{$MULTI->forms}/client_setting_form.php");
include($CFG->dirroot . '/Multitenant/helper.php');
$clientid = optional_param('id', '', PARAM_INT);
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title('Live login reports');
$PAGE->set_heading('Live login reports');
$url = new moodle_url($CFG->wwwroot . '/Multitenant/web/live_login_reports.php');
$PAGE->set_url($url);
$regions = $PAGE->blocks->get_regions();
$PAGE->blocks->add_fake_block(navigation_menu('livereports'), 'side-pre');
$PAGE->blocks->show_only_fake_blocks();
$PAGE->navbar->add('Multitenant');
$PAGE->navbar->add('Live login reports');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/Multitenant/web/styles.css'));
echo $OUTPUT->header();
$cm = new ClientManager();
$lists = $cm->get_clients();
$optionlist = array();
$optionlist['none'] = '-- Select --';
foreach ($lists as $list) {
    $optionlist[$list->get_id()] = $list->get_name();
}
$clientname = '';
$alert = 'alert alert-success';
$msg = 'Client <b>%s</b> has been selected';
if (isset($optionlist[$clientid])) {
    $clientname = $optionlist[$clientid];
}

echo $OUTPUT->single_select($url, 'id', $optionlist, $clientid, null, 'rolesform');
echo $OUTPUT->footer();
