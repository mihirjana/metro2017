<?php

class client_plugin_form extends moodleform {

//Add elements to form
    public function definition() {
        global $CFG,$DB, $PAGE, $OUTPUT;
        $mform = $this->_form;
        $pluginman = core_plugin_manager::instance();
        $plugininfo = $pluginman->get_plugins();

        $mform->addElement('text', 'client', 'Selected client', 'readonly="readonly"');
        $mform->setType('client', PARAM_NOTAGS);
        $mform->setDefault('client', $this->_customdata['clientname']);
        
        $mform->addElement('hidden', 'id', $this->_customdata['id']);
        $mform->setType('id', PARAM_TEXT);

        $mform->addElement('hidden', 'defaultdata', $this->_customdata['tmpdata']);
        $mform->setType('defaultdata', PARAM_NOTAGS);
        
        $table = new html_table();
        $table->id = 'plugins-control-panel';
        $table->attributes = array('class' => 'table table-striped');
        $table->head = array(get_string('displayname', 'core_plugin'), 'Enable/Disable');
        $table->headspan = array(1, 1, 1, 1, 1, 2, 1);
        $table->colclasses = array('pluginname', 'enable');
        $pluginsettings = array(
                        'mod' => 'modules',
                        'assignsubmission' => 'config_plugins',
                        'assignfeedback' => 'config_plugins',
                        'qtype' => 'config_plugins',
                        'block' => 'block',
                        'auth' => 'config_plugins',
                        'enrol' => 'config_plugins',
                        );
        foreach ($plugininfo as $type => $plugins) {

            if(isset($pluginsettings[$type])) {
                $heading = $pluginman->plugintype_name_plural($type);
                $pluginclass = core_plugin_manager::resolve_plugininfo_class($type);
                $mform->addElement('header',$type, $heading);          
                $mform->closeHeaderBefore($type);
                foreach ($plugins as $name => $plugin) {
                    if ($PAGE->theme->resolve_image_location('icon', $plugin->type . '_' . $plugin->name)) {
                        $icon = $OUTPUT->pix_icon('icon', '', $plugin->type . '_' . $plugin->name, array('class' => 'icon pluginicon'));
                    } else {
                        $icon = $OUTPUT->pix_icon('spacer', '', 'moodle', array('class' => 'icon pluginicon noicon'));
                    }
                    $mform->addElement('advcheckbox', $plugin->type.'_'.$plugin->name, $icon.$plugin->displayname, '',array('group'=>0), array(0, 1));
                    $mform->setType($plugin->type.'_'.$plugin->name, PARAM_INT);
                }
            }
        }
       $this->add_action_buttons(); 
    }
}
