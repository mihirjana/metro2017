<?php

/*
 * Copyright 2016 Dhruv Infoline Pvt Ltd.
 *
 * Licensed under the Moodle Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.moodle.org/licenses/MOODLE-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class courseallocation_form extends moodleform {

    private $cminstance = null;
    
    public function __construct($url, $customdata) {
        $cm = new \Multitenant\Core\Classes\ClientManager();
        $customdata['courses'] = $cm->get_client_transfer_master_courses($customdata['clientid']);
        parent::__construct($url, $customdata);
    }

    function definition() {
        $mform = & $this->_form;
        $this->_form->addElement('hidden', 'clientid', $this->_customdata['clientid']);
        $this->_form->setType('clientid', PARAM_INT);
        
        $catlist = category_tree();
        $this->category_list($catlist);

        $this->add_action_buttons(true, get_string('submit'));
    }

    /**
     * Adds a link/button that controls the checked state of a group of checkboxes.
     *
     * @param int $groupid The id of the group of advcheckboxes this element controls
     * @param string $text The text of the link. Defaults to selectallornone ("select all/none")
     * @param array $attributes associative array of HTML attributes
     * @param int $originalValue The original general state of the checkboxes before the user first clicks this element
     */
    function add_checkbox_controller($groupid, $text = null, $attributes = null, $originalValue = 0) {
        global $CFG, $PAGE;

        // Name of the controller button
        $checkboxcontrollername = 'nosubmit_checkbox_controller' . $groupid;
        $checkboxcontrollerparam = 'checkbox_controller' . $groupid;
        $checkboxgroupclass = 'checkboxgroup' . $groupid;

        // Set the default text if none was specified
        if (empty($text)) {
            $text = get_string('selectallornone', 'form');
        }

        $mform = $this->_form;
        $selectvalue = optional_param($checkboxcontrollerparam, null, PARAM_INT);
        $contollerbutton = optional_param($checkboxcontrollername, null, PARAM_ALPHAEXT);

        $newselectvalue = $selectvalue;
        if (is_null($selectvalue)) {
            $newselectvalue = $originalValue;
        } else if (!is_null($contollerbutton)) {
            $newselectvalue = (int) !$selectvalue;
        }
        // set checkbox state depending on orignal/submitted value by controoler button
        if (!is_null($contollerbutton) || is_null($selectvalue)) {
            foreach ($mform->_elements as $element) {
                if (($element instanceof MoodleQuickForm_advcheckbox) &&
                        $element->getAttribute('class') == $checkboxgroupclass &&
                        !$element->isFrozen()) {
                    $mform->setConstants(array($element->getName() => $newselectvalue));
                }
            }
        }

        $mform->addElement('hidden', $checkboxcontrollerparam, $newselectvalue, array('id' => "id_" . $checkboxcontrollerparam));
        $mform->setType($checkboxcontrollerparam, PARAM_INT);
        $mform->setConstants(array($checkboxcontrollerparam => $newselectvalue));

        $PAGE->requires->yui_module('moodle-form-checkboxcontroller', 'M.form.checkboxcontroller', array(
            array('groupid' => $groupid,
                'checkboxclass' => $checkboxgroupclass,
                'checkboxcontroller' => $checkboxcontrollerparam,
                'controllerbutton' => $checkboxcontrollername)
                )
        );

        require_once("$CFG->libdir/form/submit.php");
        $submitlink = new MoodleQuickForm_submit($checkboxcontrollername, $attributes);
        $mform->addElement($submitlink);
        $mform->registerNoSubmitButton($checkboxcontrollername);
        $mform->setDefault($checkboxcontrollername, $text);
    }

    function category_list($categories) {
        $parent = array();
        $this->_form->addElement('html', '<div class="top"  style="background:#ECEBEB;padding:">');
        $this->_form->addElement('html', '<ul style="color:blue">');
        foreach ($categories as $category) {
            static $group = 1;
            if (!empty($category['flag'])) {
                $this->_form->addElement('advcheckbox', 'cat_' . $category['id'], $category['name'], null, array('group' => $group));
                $this->category_list($category['flag']);
                $courses = get_courses($category['id']);
                $this->add_checkbox_controller($group, '<p style="margin-left: 20px!important;margin-top: 15px;position: absolute;">Select All</p>', array('style' => 'font-weight: bold;'), $group);

                $this->_form->addElement('html', '<ul style="color:green">');
                foreach ($courses as $course) {
                    $checked = array(0,1);
                    if(isset($this->_customdata['courses'][$course->id])) {
                        $checked = array(1,0);
                    }
                    $this->_form->addElement('advcheckbox', 'crs_' . $course->id, null, $course->fullname, array('group' => $group), $checked);
                }
                $this->_form->addElement('html', '</ul>');
            } else {
                $this->_form->addElement('html', '<div class="sub" style="color:red;background:#F5F5F5;">');
                $this->_form->addElement('advcheckbox', 'cat_' . $category['id'], null, $category['name'], array('group' => $group));
                $courses = get_courses($category['id']);
                $this->_form->addElement('html', '<ul style="color:green">');
                foreach ($courses as $key => $course) {
                    $checked = array(0,1);
                    if(isset($this->_customdata['courses'][$course->id])) {
                        $checked = array(1,0);
                        $parent['cat_'.$category->id] = array(1,0);
                    }
                    $this->_form->addElement('advcheckbox', 'crs_' . $course->id, null, $course->fullname, array('group' => $group), $checked);
                }
                $this->_form->addElement('html', '</ul>');
            }
        }

        $this->_form->addElement('html', '</ul>');
        // $this->add_checkbox_controller($group,$category['name'], array('style' => 'font-weight: bold;'), $group);
        //$this->add_checkbox_controller($group);
        $group++;
    }

}
