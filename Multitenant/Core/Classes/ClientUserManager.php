<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use multitenant\Client;

/**
 * Client manager Class
 * 
 */
final class Client_manager {

    private $instance;

    /**
     * this function create new client
     *
     * @param client object.
     * @return boolean 
     */
    public function create_client(Client $client) {
        
    }

    /**
     * this function return all clients
     *
     * @reurn array
     */
    public function get_clients() {
        
    }

    /**
     * this function accept one parameter
     *
     * @param int/string  client id,name
     * @return object/array of objects
     */
    public function get_client($id) {
        
    }

    /**
     * Executes a Psr\Http\Message\RequestInterface and (if applicable) automatically retries
     * when errors occur.
     *
     * @param Google_Client $client
     * @param Psr\Http\Message\RequestInterface $req
     * @return array decoded result
     * @throws Google_Service_Exception on server side error (ie: not authenticated,
     *  invalid or malformed post body, invalid url)
     */
    public function update_client(Client $client) {
        
    }

}
