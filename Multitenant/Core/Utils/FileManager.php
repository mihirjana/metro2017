<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace Multitenant\Core\Utils;

use stdClass;
use Multitenant\Core\Classes\Client;
use Multitenant\Core\Exception\FileNotFoundException;
use Multitenant\Core\Exception\DirectoryWriteException;

/**
 * Client manager Class
 * 
 */
final class FileManager {

    private $_sites_dir;
    private $_envflag = false;

    public function __construct() {
        global $CFG;
        $this->_sites_dir = $CFG->dataroot;

        //Check enviroment
        $this->setup_enviroment();
    }

    /**
     * 
     * @throws DirectoryWriteException
     * @return boolean
     */
    public function setup_enviroment() {
        if (!is_writable($this->_sites_dir)) {
            throw new DirectoryWriteException($this->_sites_dir);
        }

        if (!is_dir($this->_sites_dir . '/siteconfig')) {
            $oldmask = umask(0);
            if (!mkdir($this->_sites_dir . '/siteconfig', 0777)) {
                return true;
            }
            umask($oldmask);
        }
    }

    /**
     * this function create new client
     *
     * @param client object.
     * @return boolean 
     */
    public function create_directory($dirname) {
        if (!is_dir($this->_sites_dir . '/siteconfig/' . $dirname)) {
            mkdir($this->_sites_dir . '/siteconfig/' . $dirname, 0777);
        }
        if (!is_dir($this->_sites_dir . '/' . $dirname)) {
            mkdir($this->_sites_dir . '/' . $dirname, 0777);
        }
        
        if(is_dir($this->_sites_dir . '/siteconfig/' . $dirname) && is_dir($this->_sites_dir . '/' . $dirname)) {
            return true;
        }

        return false;
    }

    public function write_xml($xmlstr) {
        $file = fopen($this->_sites_dir . '/client.xml', "w");
        fwrite($file, $xmlstr);
        fclose($file);
        return true;
    }

    /**
     * Executes a Psr\Http\Message\RequestInterface and (if applicable) automatically retries
     * when errors occur.
     *
     * @param Google_Client $client
     * @param Psr\Http\Message\RequestInterface $req
     * @return array decoded result
     * @throws Google_Service_Exception on server side error (ie: not authenticated,
     *  invalid or malformed post body, invalid url)
     */
    public function read_xml_file() {
        if (file_exists($this->_sites_dir . '/client.xml')) {
            $reader = simplexml_load_file($this->_sites_dir . '/client.xml');
            return $reader;
        }

        $clientdb = new DbManager();
        $masterrow = $clientdb->get_master_table();
        $xml = new XmlManager();
        $xmlstr = $xml->add_client($masterrow);
        $this->write_xml($xmlstr);
        $reader = simplexml_load_file($this->_sites_dir . '/client.xml');
        return $reader;
    }
    public function folder_size($dir){
        $count_size = 0;
        $count = 0;
        if(is_dir($dir)) {
            $dir_array = scandir($dir);
            foreach($dir_array as $key=>$filename){
                if($filename!=".." && $filename!="."){
                    if(is_dir($dir."/".$filename)){
                        $new_foldersize = $this->folder_size($dir."/".$filename);
                        $count_size = $count_size+ $new_foldersize;
                    } else if(is_file($dir."/".$filename)) {
                        $count_size = $count_size + filesize($dir."/".$filename);
                        $count++;
                    }
                }
            }
        }
        return $count_size;
    }
}
