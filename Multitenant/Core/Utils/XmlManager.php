<?php

// This file is part of Lmsofindia - http://lmsofindia.com
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package multitenant\core\classes
 * @author  Shambhu Kumar {@email shambhu384@gmail.com}
 * @copyright 2016 onwards Lmsofindia {@link http://lmsofindia.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace Multitenant\Core\Utils;

use Multitenant\Core\Classes\Client;
use Multitenant\Core\Utils\SimpleXmlWriter;
use Multitenant\Core\Utils\XmlReader;

/**
 * Client manager Class
 * 
 */
final class XmlManager {

    private $_read;
    private $_write;

    public function __construct() {
        // $this->_read = new XmlReader();
        $this->_writer = new SimpleXmlWriter('clients');
    }

    public function add_client($masterrow) {
        $this->_writer->setRootName('clients');
        $this->_writer->initiate();

        foreach ($masterrow as $clients) {
            // Set branch 1-1 and its nodes
            $this->_writer->startBranch('client');
            foreach ($clients as $field => $value) {
                $this->_writer->addNode($field, $value);
            }
            $this->_writer->endBranch();
        }
        // Print the XML to screen
        return $this->_writer->getXml();
    }

}
