<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Anobody can login with any password.
 *
 * @package auth_masterssol
 * @author Martin Dougiamas
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');

/**
 * Plugin for no authentication.
 */
class auth_plugin_masterssol extends auth_plugin_base {

    /**
     * Constructor.
     */
    function auth_plugin_masterssol() {
        $this->authtype = 'masterssol';
        $this->config = get_config('auth/masterssol');
    }

    /**
     * Returns true if the username and password work or don't exist and false
     * if the user exists and the password is wrong.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
     // We don't acutally authenticate anyone, we're just here for the hooks
     function user_login ($username, $password) {
        return false;
     }
    
    /**
     * Added to work with simple signon
     *
     */
    function loginpage_hook() {
        global $SESSION; 
        $key = $this->config->masterssol_cookiesecret;
        $salt = $this->config->masterssol_cookiesalt;

        if(isset($_COOKIE['SimpleSSOCookie'])) {
            $data = base64_decode($_COOKIE['SimpleSSOCookie']);
            $dec_val = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_CBC, $salt);
            $retval = rtrim($dec_val, "\0\4");
            $arraytemp = explode('|',$retval);

            $usernametemp = explode('=',$arraytemp[0]);
       
			$emailtemp = explode('=',$arraytemp[1]);
            $user = get_complete_user_data('username', $usernametemp[1]);

			if ($user) {
            
				complete_user_login($user);
			
				if (empty($SESSION->wantsurl)) {
					$SESSION->wantsurl = null;
					$referer = get_local_referer(false);
					if ($referer && $referer != $CFG->wwwroot &&
						$referer != $CFG->wwwroot . '/' &&
						$referer != $CFG->httpswwwroot . '/login/' &&
						strpos($referer, $CFG->httpswwwroot . '/login/?') !== 0 &&
						strpos($referer, $CFG->httpswwwroot . '/login/index.php') !== 0) { // There might be some extra params such as ?lang=.
						$SESSION->wantsurl = $referer;
					}
				}
				

			   redirect($SESSION->wantsurl);
		   
			}
       }
     }
    /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object
     * @param  string  $newpassword Plaintext password
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        return false;
    }

    function prevent_local_passwords() {
        return true;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return true;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return false;
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null;
    }

    /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return false;
    }

    /**
     * Returns true if plugin can be manually set.
     *
     * @return bool
     */
    function can_be_manually_set() {
        return false;
    }
    
    // Defaults for configuration values.
    function get_defaults() {
        return array (
            'masterssol_cookiename' => get_string('masterssol_cookiename_default', 'auth_masterssol'),
            'masterssol_cookiepath' => get_string('masterssol_cookiepath_default', 'auth_masterssol'),
            'masterssol_cookiedomain' => get_string('masterssol_cookiedomain_default', 'auth_masterssol'),
            'masterssol_cookieexpiry' => get_string('masterssol_cookieexpiry_default', 'auth_masterssol'),
            'masterssol_cookiesecret' => get_string('masterssol_cookiesecret_default', 'auth_masterssol'),
            'masterssol_cookiesalt' => get_string('masterssol_cookiesalt_default', 'auth_masterssol')
        );
    }

    function config_form($config, $err, $user_fields) {
        foreach ($this->get_defaults() as $key => $value) {
            if (isset($config->$key)) continue;
                $config->$key = $value;
            }
            include dirname(__FILE__).'/config.php';
     }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config) {
        global      $CFG;
            $defaults = $this->get_defaults();
                foreach ($defaults as $key => $value) {
				  if (isset($config->$key)) continue;
						$config->$key = $value;
							}

								foreach (array_keys($defaults) as $key) {
								  set_config($key, $config->$key, 'auth/masterssol');
									  }
								return true;

    }

}


