<?php

/*
  Copyright 2012 Marvin Pinto (me@marvinp.ca)

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License.  You may obtain a copy of
  the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
  License for the specific language governing permissions and limitations under
  the License.
*/


global $CFG;
?>
<table cellpadding="9" cellspacing="0" >

  <tr valign="top">
  <td align="right"><?php print_string('masterssol_cookiename_label', 'auth_masterssol') ?></td>
  <td><input name="masterssol_cookiename" type="text" value="<?php echo htmlspecialchars($config->masterssol_cookiename) ?>" /></td>
  <td><?php print_string('masterssol_cookiename_desc', 'auth_masterssol') ?></td>
  </tr>


  <tr valign="top">
  <td align="right"><?php print_string('masterssol_cookiepath_label', 'auth_masterssol') ?></td>
  <td><input name="masterssol_cookiepath" type="text" value="<?php echo htmlspecialchars($config->masterssol_cookiepath) ?>" /></td>
  <td><?php print_string('masterssol_cookiepath_desc', 'auth_masterssol') ?></td>
  </tr>


  <tr valign="top">
  <td align="right"><?php print_string('masterssol_cookiedomain_label', 'auth_masterssol') ?></td>
  <td><input name="masterssol_cookiedomain" type="text" value="<?php echo htmlspecialchars($config->masterssol_cookiedomain) ?>" /></td>
  <td><?php print_string('masterssol_cookiedomain_desc', 'auth_masterssol') ?></td>
  </tr>


  <tr valign="top">
  <td align="right"><?php print_string('masterssol_cookieexpiry_label', 'auth_masterssol') ?></td>
  <td><input name="masterssol_cookieexpiry" type="text" value="<?php echo htmlspecialchars($config->masterssol_cookieexpiry) ?>" /></td>
  <td><?php print_string('masterssol_cookieexpiry_desc', 'auth_masterssol') ?></td>
  </tr>


  <tr valign="top">
  <td align="right"><?php print_string('masterssol_cookiesecret_label', 'auth_masterssol') ?></td>
  <td><input name="masterssol_cookiesecret" type="password" value="<?php echo htmlspecialchars($config->masterssol_cookiesecret) ?>" /></td>
  <td><?php print_string('masterssol_cookiesecret_desc', 'auth_masterssol') ?></td>
  </tr>


  <tr valign="top">
  <td align="right"><?php print_string('masterssol_cookiesalt_label', 'auth_masterssol') ?></td>
  <td><input name="masterssol_cookiesalt" type="text" value="<?php echo htmlspecialchars($config->masterssol_cookiesalt) ?>" /></td>
  <td><?php print_string('masterssol_cookiesalt_desc', 'auth_masterssol') ?></td>
  </tr>


  <tr><td><br/></td></tr>

  <tr valign="top">
  <td align="right"><?php print_string('masterssol_update_label', 'auth_masterssol') ?></td>
  <td colspan="2">
  <p><?php print_string('masterssol_update_desc', 'auth_masterssol') ?></p></td>
  </tr>
  </table>

