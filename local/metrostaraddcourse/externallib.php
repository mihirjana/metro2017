<?php

require_once("../../config.php");

global $CFG,$DB;

require_once($CFG->libdir . "/externallib.php");
require_once($CFG->dirroot. "/course/lib.php");
require_once($CFG->dirroot. "/cohort/externallib.php");
require_once($CFG->dirroot. "/cohort/lib.php");
require_once($CFG->dirroot. "/enrol/manual/externallib.php");
require_once($CFG->dirroot. "/enrol/locallib.php");
require_once($CFG->dirroot. "/enrol/cohort/locallib.php");
require_once($CFG->dirroot. "/blocks/cbsi/lib.php");

// print_r($_POST);
// die(__FILE__.":".__LINE__);

class local_metrostaraddcourse_external extends external_api {

   public static function add_course_parameters() {
       return new external_function_parameters(
           array(
            'selecting_cohort_names'=> new external_value(PARAM_RAW, 'Cohort enrollments',VALUE_DEFAULT, null),
            'cohort_selection_response'=> new external_value(PARAM_RAW, 'Specific cohort names',VALUE_DEFAULT, null),
            'coursenofsections'=> new external_value(PARAM_RAW, 'Number of Course sections',VALUE_DEFAULT, 10),
            'id' => new external_value(PARAM_RAW, 'Course id',VALUE_DEFAULT, null),
            'category' => new external_value(PARAM_INT, 'Course category',VALUE_REQUIRED),
            'sortorder' => new external_value(PARAM_RAW, 'Course position in category list of coruses',VALUE_DEFAULT, null),
            'fullname' => new external_value(PARAM_RAW, 'Course long name',VALUE_REQUIRED),
            'shortname' => new external_value(PARAM_RAW, 'Course shortname',VALUE_REQUIRED),
            'idnumber' => new external_value(PARAM_RAW, 'Idnumber of course',VALUE_DEFAULT, null),
            'summary' => new external_value(PARAM_RAW, 'Description of course',VALUE_REQUIRED),
            'summaryformat' => new external_value(PARAM_INT, 'Format of summary (e.g. HTML)',VALUE_DEFAULT,'1'),
            'format' => new external_value(PARAM_RAW, 'Course format (e.g. topics, weekly, etc)',VALUE_DEFAULT,'topics'),
            'showgrades' => new external_value(PARAM_INT, 'Show grades to students of course',VALUE_REQUIRED),
            'newsitems' => new external_value(PARAM_INT, 'Allow news items block in course',VALUE_REQUIRED),
            'startdate' => new external_value(PARAM_RAW,'Date that course becomes available to students.',VALUE_REQUIRED),
            'marker' => new external_value(PARAM_INT, 'Course marker',VALUE_REQUIRED),
            'maxbytes' => new external_value(PARAM_INT, 'Restricts course to this number of bytes',VALUE_REQUIRED),
            'legacyfiles' => new external_value(PARAM_INT, 'Allow or not legacyfiles',VALUE_DEFAULT,'1'),
            'showreports' => new external_value(PARAM_INT, 'Show or not default reports',VALUE_REQUIRED),
            'visible' => new external_value(PARAM_INT, 'Make the course visible now or not',VALUE_REQUIRED),
            'visibleold' => new external_value(PARAM_INT, 'Old version of the visbility variable',VALUE_REQUIRED),
            'groupmode' => new external_value(PARAM_INT, 'Allow groups',VALUE_REQUIRED),
            'groupmodeforce' => new external_value(PARAM_INT, 'Force groups',VALUE_REQUIRED),
            'defaultgroupingid' => new external_value(PARAM_INT, 'Default group id',VALUE_REQUIRED),
            'lang' => new external_value(PARAM_RAW, 'Force language for course',VALUE_REQUIRED),
            'calendartype' => new external_value(PARAM_RAW, 'Calendar format if used in course',VALUE_REQUIRED),
            'theme' => new external_value(PARAM_RAW, 'Theme used for this course',VALUE_REQUIRED),
            'timecreated' => new external_value(PARAM_NUMBER, 'Time stamp when course was created',VALUE_REQUIRED),
            'timemodified' => new external_value(PARAM_NUMBER, 'Time stamp of any changes',VALUE_REQUIRED),
            'requested' => new external_value(PARAM_INT, 'Allow course to be requested',VALUE_REQUIRED),
            'enablecompletion' => new external_value(PARAM_INT, 'Turn off or on course completion tracking',VALUE_REQUIRED),
            'completionnotify' => new external_value(PARAM_INT, 'Turn on or off notifications for those when someone completes course',VALUE_REQUIRED),
            'cacherev' => new external_value(PARAM_INT, 'Time stamp to refresh cache',VALUE_REQUIRED),
            )
        );
    }

    public static function add_course(
        $selecting_cohort_names,
        $cohort_selection_response,
        $coursenofsections,
        $id,
        $category,
        $sortorder,
        $fullname,
        $shortname,
        $idnumber,
        $summary,
        $summaryformat,
        $format,
        $showgrades,
        $newsitems,
        $startdate,
        $marker,
        $maxbytes,
        $legacyfiles,
        $showreports,
        $visible,
        $visibleold,
        $groupmode,
        $groupmodeforce,
        $defaultgroupingid,
        $lang,
        $calendartype,
        $theme,
        $timecreated,
        $timemodified,
        $requested,
        $enablecompletion,
        $completionnotify,
        $cacherev) {

        global $CFG,$USER,$DB,$PAGE;

        $params = self::validate_parameters(self::add_course_parameters(),
                array(
                        'selecting_cohort_names'=>$selecting_cohort_names,
                        'cohort_selection_response'=>urldecode($cohort_selection_response),
                        'coursenofsections'=>$coursenofsections,
                        'id'=>$id,
                        'category'=>$category,
                        'sortorder'=>$sortorder,
                        'fullname'=>$fullname,
                        'shortname'=>$shortname,
                        'idnumber'=>$idnumber,
                        'summary'=>$summary,
                        'summaryformat'=>$summaryformat,
                        'format'=>$format,
                        'showgrades'=>$showgrades,
                        'newsitems'=>$newsitems,
                        'startdate'=>$startdate,
                        'marker'=>$marker,
                        'maxbytes'=>$maxbytes,
                        'legacyfiles'=>$legacyfiles,
                        'showreports'=>$showreports,
                        'visible'=>$visible,
                        'visibleold'=>$visibleold,
                        'groupmode'=>$groupmode,
                        'groupmodeforce'=>$groupmodeforce,
                        'defaultgroupingid'=>$defaultgroupingid,
                        'lang'=>$lang,
                        'calendartype'=>$calendartype,
                        'theme'=>$theme,
                        'timecreated'=>$timecreated=$timecreated,
                        'timemodified'=>$timemodified,
                        'requested'=>$requested,
                        'enablecompletion'=>$enablecompletion,
                        'completionnotify'=>$completionnotify,
                        'cacherev'=>$cacherev));

        $transaction = $DB->start_delegated_transaction();

        if ($targetedCourse=$DB->get_record_sql("
            select *
            from mdl_course
            where fullname='".$fullname."'
            and shortname='".$shortname."'
            and category='".$category."'"))
        {
            throw new invalid_parameter_exception('course with the same name in that category already exists');
        }

        $context = context_user::instance($USER->id);
        self::validate_context($context);


        if (!has_capability('moodle/course:create', $context))
        {
            throw new moodle_exception('cannotmanageuser');
        }

        $data = new stdClass();
        $data->id=$id;
        $data->category=$category;
        $data->sortorder=$sortorder;
        $data->fullname=urldecode($fullname);
        $data->shortname=urldecode($shortname);
        $data->idnumber=$idnumber;
        $data->summary=urldecode($summary);
        $data->summaryformat=$summaryformat;

        //$data->format=$format;
        //if category is meeting, set course format to singleactivity
        if($category == "Meeting")
        {
            $data->format = "singleactivity";
        } else {
            $data->format=$format; //default of topics
        }

        $data->showgrades=$showgrades;
        $data->newsitems=$newsitems;
        $ut_time=$DB->get_record_sql("select UNIX_TIMESTAMP('".urldecode($startdate)."') as utstartdate");
        $data->startdate=$ut_time->utstartdate;
        $data->marker=$marker;
        $data->maxbytes=$maxbytes;
        $data->legacyfiles=$legacyfiles;
        $data->showreports=$showreports;
        $data->visible=$visible;
        $data->visibleold=$visibleold;
        $data->groupmode=$groupmode;
        $data->groupmodeforce=$groupmodeforce;
        $data->defaultgroupingid=$defaultgroupingid;
        $data->lang=$lang;
        $data->calendartype=$calendartype;
        $data->theme=$theme;
        $data->timecreated=$timecreated;
        $data->timemodified=$timemodified;
        $data->requested=$requested;
        $data->enablecompletion=$enablecompletion;
        $data->completionnotify=$completionnotify;
        $data->cacherev=$cacherev;
/*
        $debug = "
        Cohort names: $selecting_cohort_names
        Cohort slection response: $cohort_selection_response
        Course number of sections: $coursenofsections
        ID: $id
        Category: $category
        Sort order: $sortorder
        Fullname: $fullname
        Shortname :$shortname
        IDNUMBER: $idnumber
        Summary: $summary
        Summary Format: $summaryformat
        Format: $format
        Show Grades: $showgrades
        News Items: $newsitems
        Start Date: $startdate
        Marker: $marker
        Max Bytes: $maxbytes
        Legacy Files: $legacyfiles
        Show Reports: $showreports
        Visible: $visible
        Visible Old: $visibleold
        Groupe Mode: $groupmode
        Groupe Mode Force: $groupmodeforce
        Default Grouping ID: $defaultgroupingid
        Language: $lang
        Calendar Type: $calendartype
        Theme: $theme
        Time Created: $timecreated
        Time Modified: $timemodified
        Requested: $requested
        Enable Completion: $enablecompletion
        Completion Notify: $completionnotify
        Cache Rev: $cacherev
        ";

        // self::debug('$debug', $debug, __LINE__);
*/
        $newcourse=create_course($data);
        $transaction->allow_commit();

        //before returning new course id
        //update the number of sections
        //per user input or default set above
        //these are created with the create_course
        //function hence the update_record call
        $data = new stdClass();
        $get_id=$DB->get_record_sql("
            select id
            from mdl_course_format_options
            where name='numsections'
            and courseid=".$newcourse->id);

        $data->id = $get_id->id;
        $data->courseid=$newcourse->id;
        $data->name='numsections';
        $data->value=$coursenofsections;
        $DB->update_record('course_format_options',$data);

        //enroll course creator into course
        // $enrollments = array(
        //   array(
        //     'roleid'    => 9,
        //     'userid'    => $USER->id,
        //     'courseid'  => $newcourse->id,
        //     'timestart' => null,
        //     'timeend'   => null,
        //     'suspend'   => null)
        //   );

        // enrol_manual_external::enrol_users($enrollments);

        if(urldecode($cohort_selection_response)=='Yes All') {
            $cohorts = file_get_contents("http://cbsi-connect.net/local/metrostargetcohort/client/client.php");
            $cohorts  = json_decode($cohorts, true);

            foreach($cohorts as $cohort) {
                cohort_add_course($newcourse->id, $cohort['id']);
            }
        }

        elseif(urldecode($cohort_selection_response) == 'Yes Selected') {
            $selecting_cohort_names = explode(",",urldecode($selecting_cohort_names));

            //self::debug('$selecting_cohort_names', $selecting_cohort_names, __LINE__);

            //get duplicate selections from bad session handling?
            $selecting_cohort_names = array_unique($selecting_cohort_names);

            $selected_cohorts = $selecting_cohort_names;

            foreach($selected_cohorts as $cohort) {
                //cohort_add_course($courseid, $cohortid)
                try {
                    cohort_add_course($newcourse->id, $cohort);
                } catch (Exception $e) {
                    die(__FILE__.":".__LINE__."<br>". $e->getMessage());
                }
            }
        }

        return $newcourse->id;
}

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function add_course_returns() {
        return new external_value(PARAM_TEXT, 'Adds a course to system and returns its ID');
    }


    /**
     * @param course
     * -Course object returned from Moodle after a course is created
     * -Takes backup of attendance activity and adds it to newly created course
     */

    /**
     * @param $name of variable testing
     * @param $bug value of variable testing
     * @param $line of code test called from
     */
    public static function debug($name, $bug, $line = '') {
      global $CFG;

      $log = __FILE__ . "\\";

      if(is_array($bug)) {
        $bug = print_r($bug);
      }

      if(is_object($bug)) {
        $bug = var_dump($bug);
      }

      $log .= "\nLine: $line\n";
      $log .= "$name: \n $bug\n";

      file_put_contents('/var/www/html/moodledata/client_externallib_debug.log', $log, FILE_APPEND | LOCK_EX);
    }

    public static function cohort_add_course($courseid, $cohortid) {

        global $DB,$CFG;

        if (!enrol_is_enabled('cohort')) {
            // Not enabled.
            return false;
        }

        if ($DB->record_exists('enrol', array('courseid' => $courseid, 'enrol' => 'cohort'))) {
            // The course already has a cohort enrol method.
            return false;
        }

        // Get the cohort enrol plugin
        $enrol = enrol_get_plugin('cohort');

        // Get the course record.
        $course = $DB->get_record('course', array('id' => $courseid));

        // Add a cohort instance to the course.
        $instance = array();
        $instance['name'] = '';
        $instance['status'] = ENROL_INSTANCE_ENABLED; // Enable it.
        $instance['customint1'] = $cohortid; // Used to store the cohort id.
        $instance['roleid'] = $enrol->get_config('roleid'); // Default role for cohort enrol which is usually student.
        $instance['customint2'] = 0; // Optional group id.
        $enrol->add_instance($course, $instance);

        // Sync the existing cohort members.
        $trace = new null_progress_trace();
        enrol_cohort_sync($trace, $course->id);
        $trace->finished();
    }
}
