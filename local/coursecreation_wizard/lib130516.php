<?php
defined('MOODLE_INTERNAL') || die();
include_once($CFG->dirroot . '/course/lib.php');

/*
|------------------------------
| Course creation function
|
| @return array
|------------------------------
*/
function course_creation($category, $fullname, $summary,$subcategory)
{   
	global $DB, $USER;
	$insert = (object) array('category' => $category, 'fullname' => $fullname, 'summary' => $summary,'shortname'=>$fullname);

	if($course = create_course($insert))
	{   
		$insert12 = (object) array('courseid'=>$course->id,'course_creator'=>$USER->id,'default_shortname'=>$course->fullname,'categoryid' => $category, 'parent_category' => $subcategory);

		$DB->insert_record('block_cbsi', $insert12);

    	$data = array('status'=>true, 'id'=> $course->id, 'fullname'=> $course->fullname); 
	}
	else
	{
	    $data = array('status'=>false, 'message'=>'can\'t create course try later'); 
	}
	return $data;
}

/*
|------------------------------
| Course time modify
|
| @return array
|------------------------------
*/
function course_time_modify($courseid, $datestamp,$timestamp,$enddate,$endtime)
{
	global $DB;
	$date = $datestamp.$timestamp;
	$stdate = strtotime($date);
	$endstampdate = $enddate.$endtime;
	$stenddate = strtotime($endstampdate);
	$cseid = $DB->get_record('block_cbsi',array('courseid' =>$courseid));
	//var_dump(expression)
	$update = (object) array('id' => $courseid, 'startdate' => $stdate);
	$update12 = (object) array('id' => $cseid->id, 'start_datetime' => $stdate,'end_datetime'=>$stenddate);
	// print_object($update12);
	// exit();
	if($DB->update_record('course', $update)){

		if($DB->update_record('block_cbsi', $update12)){
			return array('status' => true);
		} else {
			return array('status' => false);

		}
		
	}
	
}

/*
|------------------------------
| Course delete
|
| @return array
|------------------------------
*/
function course_finaldelete($courseid)
{
	global $DB;
	if($DB->delete_records('course', array('id' => $courseid)))
	{ 
		$DB->delete_records('block_cbsi', array('courseid' => $courseid));
    	$data = array('status'=>true); 
	}
	else
	{
	    $data = array('status'=>false); 
	}
	return $data;
}

/*
|------------------------------
| Navigation
|
| @return array
|------------------------------
*/
function  local_coursecreation_wizard_extend_settings_navigation($settingsnav, $context){
	// global $CFG, $COURSE, $courseid, $DB;
	
// $course = $DB->get_record('course', array('id' => $COURSE->id));	
// //require_course_login($course);
// $context = context_course::instance($course->id);
// 	if (has_capability('moodle/role:assign', $context)) {
// 		$coursenode = $settingsnav->get('courseadmin');
// 		if($coursenode)
// 		{
//             $url = "$CFG->wwwroot/local/course_batches/view_batch.php?id=$COURSE->id";
            
//             //for batch
//             $coursenode->add(get_string('batch', 'local_course_batches'), null,
//                         navigation_node::TYPE_CONTAINER, null, 'batch',
//                         new pix_icon('i/badge', get_string('batch', 'local_course_batches')));
            
// //            $url = "$CFG->wwwroot/local/course_batches/batch.php?courseid=$COURSE->id";
// //
// //			$coursenode->get('batch')->add(get_string('addbatch', 'local_course_batches'), $url,
// //				navigation_node::TYPE_SETTING, null, 'batch');            

//             $coursenode->get('batch')->add(get_string('viewbatch', 'local_course_batches'), $url,
//                     navigation_node::TYPE_SETTING, null, 'course_batches');

//             //for batch
//             /* 
//             $url = "$CFG->wwwroot/local/course_batches/batch.php?courseid=$COURSE->id";

//             $coursenode->get('coursepromo')->add(get_string('addbatch', 'local_course_batches'), $url,
//             navigation_node::TYPE_SETTING, null, 'coursepromo');


//            $url = "$CFG->wwwroot/local/course_batches/view_batch.php?id=$COURSE->id";
//            $coursenode->get('coursepromo')->add(get_string('viewbatch', 'local_course_batches'), $url,
//                    navigation_node::TYPE_SETTING, null, 'course_batches'); 
//             */
// 		}
// 	}	
}