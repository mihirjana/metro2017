<?php
$string['pluginname'] = 'Asistente de la Creación de Cursos de CBSI-Connect';
$string['whatwant'] = '¿Qué es lo que usted desea hacer hoy?';
$string['Creatingnew'] = 'Crear una reunión o capacitación nueva';
$string['whatwantclass'] = '¿Qué tipo de curso desea crear?';
$string['whatlanguage'] = 'En que lenguage estará disponible?';
$string['Whattitle'] = 'Cual es el Título?';
$string['entertitle'] = 'Ingrese Título';
$string['whatdesc'] = '¿Cuál es la descripción?';
$string['enterdesc'] = 'Ingrese descripción ';
$string['Setdate'] = 'Establezca la fecha y hora de inicio y la fecha y hora de finalización.';
$string['whattrain'] = 'Ingrese la fecha de inicio';
$string['Choosetime'] = 'Ingrese la hora de inicio';
$string['Choosetimeend'] = 'Ingrese la hora de finalización';
$string['trainingsetup'] = '¡Felicidades! Su formación o reunión está configurado. ¿Qué le gustaría hacer ahora?';
$string['SaveHomePage'] = 'Guardar y volver a la Página de Inicio';
$string['DeletePage'] = 'Eliminar y volver a la Página de Inicio';
$string['GoPage'] = 'Guardar y Ver Curso';
$string['SaveUp'] = 'Guardar y Configurar';
$string['whattrainend'] = 'Ingrese la fecha de finalización';

$string['step1'] = 'Paso1';
$string['step2'] = 'Paso2';
$string['step3'] = 'Paso3';
$string['step4'] = 'Paso4';
$string['step5'] = 'Paso5';
$string['step6'] = 'Paso6';
$string['next'] = 'Siguiente';
$string['setdate2'] = 'Establezca Fecha';
$string['decidelater'] = 'Decidir luego';
$string['or'] = 'O';
$string['wizardmeeting'] = 'Reunión';
$string['wizardtraining'] = 'Capacitación';


?>
