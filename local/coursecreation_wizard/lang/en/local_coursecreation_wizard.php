<?php
$string['pluginname'] = 'CBSI-Connect Course Creation Wizard';
$string['whatwant'] = 'What do you want to do today ?';
$string['Creatingnew'] = 'Creating a new meeting or training';
$string['whatwantclass'] = 'What type of class do you want to create?';
$string['whatlanguage'] = 'What language will this be available in ?';
$string['Whattitle'] = 'What is title ?';
$string['entertitle'] = 'Enter title';
$string['whatdesc'] = 'What is the description ? ';
$string['enterdesc'] = 'Enter description';
$string['Setdate'] = 'Set the start date of training and meeting? ';
$string['whattrain'] = 'When do you want this training or meeting begin ?';
$string['Choosetime'] = 'Choose a begin time ?';
$string['Choosetimeend'] = 'Choose a end time ?';
$string['trainingsetup'] = 'Congratulation your meeting or training is setup. What would you like  to do  now ?';
$string['SaveHomePage'] = 'Save and Go Back Home Page';
$string['DeletePage'] = 'Delete and Go Back Home Page';
$string['GoPage'] = 'Save and View Course ';
$string['SaveUp'] = 'Save and Start Set Up';
$string['whattrainend'] = 'When do you want this training or meeting end ?';

$string['step1'] = 'Step1';
$string['step2'] = 'Step2';
$string['step3'] = 'Step3';
$string['step4'] = 'Step4';
$string['step5'] = 'Step5';
$string['step6'] = 'Step6';
$string['next'] = 'NEXT';
$string['setdate2'] = 'Set Date';
$string['decidelater'] = 'Decide Later';
$string['or'] = 'OR';
$string['wizardmeeting'] = 'Meeting';
$string['wizardtraining'] = 'Training';


?>
