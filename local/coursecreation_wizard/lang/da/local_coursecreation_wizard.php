<?php
$string['pluginname'] = 'CBSI-Connect Cursus Creatie Wizard';
$string['whatwant'] = 'Wat wilt u vandaag doen? ';
$string['Creatingnew'] = 'Creër een nieuwe vergadering of een training';
$string['whatwantclass'] = 'Welke type cursus wenst u te creëren?';
$string['whatlanguage'] = 'What language will this be available in ?';
$string['Whattitle'] = 'Wat is de titel?';
$string['entertitle'] = 'Ente titel';
$string['whatdesc'] = 'Wat is de Beschrijving? ';
$string['enterdesc'] = 'Ente Beschrijving';
$string['Setdate'] = 'Wat is de aanvangsdatum van de training en de vergadering? ';
$string['whattrain'] = 'Wanneer wilt u dat deze training of vergadering aanvangt?';
$string['Choosetime'] = 'kies de tijd om te beginnen';
$string['Choosetimeend'] = 'Kies de tijd om te beëindigen';
$string['trainingsetup'] = 'Gefeliciteerd uw vergadering is gecreërd. Wat wilt u nu doen?';
$string['SaveHomePage'] = 'Opslaan en terug naar startpagina';
$string['DeletePage'] = 'Wissen en terug naar startpagina';
$string['GoPage'] = 'Opslaan en weergeven natuurlijk ';
$string['SaveUp'] = 'Opslaan en Start Instellingen';
$string['whattrainend'] = 'When do you want this training or meeting end ?';

$string['step1'] = 'Stap1';
$string['step2'] = 'Stap2';
$string['step3'] = 'Stap3';
$string['step4'] = 'Stap4';
$string['step5'] = 'Stap5';
$string['step6'] = 'Stap6';
$string['next'] = 'Volgende';
$string['setdate2'] = 'Datum instellen';
$string['decidelater'] = 'Decide Later';
$string['or'] = 'Of';
$string['wizardmeeting'] = 'Vergadering';
$string['wizardtraining'] = 'Training';


?>
