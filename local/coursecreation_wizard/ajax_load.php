<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

header('Content-Type:application/json');
require(dirname(dirname(dirname(__FILE__))).'/config.php');
include_once('lib.php');

$callback = trim($_REQUEST['callback']);
$params = array();
unset($_REQUEST['callback']);
foreach ($_REQUEST as $field => $value)
{
    $params[$field] = $value;
}
if(isloggedin())
{
    if(is_callable($callback))
    {
        $response = call_user_func_array($callback, $params);
    }
    else
    {
        $response = array('status'=> false, 'error' => 'function not exists please check.');
    }
}
else
{
    $response = array('status'=> false, 'error' => 'Please login first');
}
echo json_encode($response);