<?php
/**
 * This script runs along with cron to sync the users from other tenants
 * and insert into mdl_metro_user_report table.
 *
 * @package MetroSync
 * @copyright CBSI-Connect Metro
 * @date 02-aug-2016
 */
namespace local_metrorpt\task;
include_once $CFG->dirroot . '/user/profile/lib.php';

class cron_task extends \core\task\scheduled_task
{
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name()
    {
        return get_string('pluginname', 'local_metrorpt');
    }

    /**
     * Sync users information across tenant
     *
     * @throws dml_connnection_exception
     * @return array
     */
    public function execute()
    {
        global $CFG, $DB;
		$tempdb = null;
		$tempdb = $DB;
        mtrace('Sync started');
				if(!$lastsyncdate = $DB->get_field('config', 'value', ['name' => 'metrolastsync'])) {
						$lastsyncdate = 0;
				}
        $tenants = $DB->get_records('multitenant_master', null, '', 'id, sub_domain');
        // Find user from tenants
        foreach($tenants as $tenant) {
            $class = get_class($DB);
            $externaldb = new $class();
            try {
				
				// Connect tenant database
				if ($tenant->sub_domain == 'a1') { $tenant->sub_domain = 'aone';}
				if ($tenant->sub_domain == 'a2') { $tenant->sub_domain = 'atwo';}
				
                // Connect tenant database
                $externaldb->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, 'moodle_' . $tenant->sub_domain, $CFG->prefix, $CFG->dboptions);
            } catch(dml_connection_exception $e) {
                throw $e;
            }

			$DB = $externaldb;
			
            $users = $externaldb->get_records_select('user', ' (deleted = 0 and suspended = 0) and  (timemodified > '.$lastsyncdate. ' and username != "guest")');
            $admins = $externaldb->get_field('config', 'value', array(
                'name' => 'siteadmins'
            ));
            foreach($users as $user) {
                profile_load_data($user);
                $admin = explode(',', $admins);

								if (in_array($user->id, $admin)) {
                    $user->role = 'admin';
                }
								if(!isset($user->profile_field_gender)) {
										$user->profile_field_gender = '';
								}
							  if(!isset($user->role)) {
										$user->role = '';
								}
                $user->tenantid = $tenant->id;
								$user->userid = $user->id;
            }
						$courses = $externaldb->get_records_select('course', 'category !=0');
						$categories = $externaldb->get_records('course_categories');
						foreach($courses as &$course) {
								$course->courseid = $course->id;
								$course->tenantid= $tenant->id;
								$course->categoryname = $categories[$course->category]->name;
						}
						$sql = 'SELECT ra.id as roleid, c.id as courseid, u.id as userid,ue.timestart, ue.timeend,ue.timemodified as timemodified FROM {course} c '.
									 'JOIN {context} ct ON c.id = ct.instanceid '.
									 'JOIN {role_assignments} ra ON ra.contextid = ct.id '.
									 'JOIN {user} u ON u.id = ra.userid '.
									 'JOIN {role} r ON r.id = ra.roleid '.
									 'JOIN {enrol} e ON e.courseid = c.id '.
									 'JOIN {user_enrolments} ue on ue.enrolid = e.id  WHERE (c.id in('.implode(',', array_keys($courses)).') and ct.contextlevel = 50) and (e.roleid=5 and ue.userid = u.id and ue.timemodified > '.$lastsyncdate.' and u.deleted = 0 and u.suspended = 0)';
						$enrolusers = $externaldb->get_records_sql($sql);

						array_map(function(& $row){
							unset($row->roleid);
						}, $enrolusers);

						$tenantenrolusers[$tenant->id] = $enrolusers;

						$tenantcourses[$tenant->id] = $courses;
            $tenantusers[$tenant->id] = $users;
        }
        // Identify update / insert users
		// Identify update / insert users
		$DB = $tempdb;
        foreach($tenantusers as $tenantid => $users) {
            foreach($users as $user) {
                $existsuser = $DB->get_record('metro_user_report', ['username' => $user->username, 'tenantid' => $tenantid]);
                // check user if exists then update else insert
                if ($existsuser) {
                    $user->id = $existsuser->id;

                    // check any diff between exitsing user to tenant user
                    $updateflag = false;
                    foreach($existsuser as $field => $value) {
												if(isset($user->$field)) {
                        		if ($user->$field != $value) {
                            		$updateflag = true;
                        		}
											  }
                    }
                    if ($updateflag) {
                        $user->id = $existsuser->id;
                        $updateusers[$user->id] = $user;
                        continue;
                    }
                    continue;
                }
                // New user from tenants
                unset($user->id);
                $newusers[] = $user;
            }
        }

				// Identify update / insert courses
        foreach($tenantcourses as $tenantid => $courses) {
            foreach($courses as $course) {
                $existscourse = $DB->get_record('metro_course_report', ['courseid' => $course->courseid, 'tenantid' => $tenantid]);
                // check user if exists then update else insert
                if ($existscourse) {
                    $course->id = $existscourse->id;
                    // check any diff between exitsing user to tenant user
                    $updateflag = false;

										foreach($existscourse as $field => $value) {
												if(isset($course->$field)) {
                        		if ($course->$field != $value) {
                            	$updateflag = true;
                        		}
												}
                    }
                    if ($updateflag) {
                        $course->id = $existscourse->id;
                        $updatecourses[$course->id] = $course;
                        continue;
                    }
                    continue;
                }
                // New user from tenants
                unset($course->id);
								$course->tenantid = $tenantid;
                $newcourses[$course->tenantid.$course->courseid] = $course;
            }
        }


        // check enrolled users
        foreach($tenantenrolusers as $tenantid => $rows) {
            foreach($rows as $row) {
                $isenrolled = $DB->get_record('metro_user_course_enrol_report', ['userid' => $row->userid, 'courseid' => $row->courseid, 'tenantid' => $tenantid]);
                // check if not enrolled then enrol
                $updateflag = false;
                if ($isenrolled) {
                    $row->id = $isenrolled->id;
                    foreach($row as $field => $value) {
                        if (isset($row->field)) {
                            if ($row->$field == $value) {
                                $updateflag = true;
																continue;
                            }
                        }
                    }

				            if ($updateflag) {
				                $row->id = $isenrolled->id;
				                $enrolupdateusers[] = $row;
												continue;
				            }
								}
                // Insert new user enrollment
                unset($row->id);
                $row->tenantid = $tenantid;
                $enrolnewusers[] = $row;
            }
        }
        // insert & update all users
				$info = '';
        try {
            if (isset($updateusers)) {
                foreach($updateusers as $user) {
                    $DB->update_record('metro_user_report', $user);
                }
								$info .= ' Update users '.count($updateusers);
            }

            if (isset($newusers)) {
                foreach($newusers as $user) {
                    $DB->insert_record('metro_user_report', $user);
                }
								$info .= '| New users '.count($newusers);
            }

						if(isset($newcourses)) {
								foreach($newcourses as $insert) {
										$DB->insert_record('metro_course_report', $insert);
								}
								$info .= '| New courses '.count($newcourses);
						}
						if(isset($updatecourses)) {
								foreach($updatecourses as $update) {
										$DB->update_record('metro_course_report', $update);
								}
								$info .= '| Update courses '.count($updatecourses);
						}

						if(isset($enrolnewusers)) {
								foreach($enrolnewusers as $enroluser) {
										$DB->insert_record('metro_user_course_enrol_report', $enroluser);
								}
								$info .= '| New enrollment '.count($enrolnewusers);
						}
						if(isset($enrolupdateusers)) {
								foreach($enrolupdateusers as $enroluser) {
										$DB->update_record('metro_user_course_enrol_report', $enroluser);
								}
								$info .= '| Update enrollment '.count($updatecourses);
						}
        } catch(dml_exception $e) {
            throw $e;
        }
				if($lastsyncdate == 0) {
						$DB->insert_record('config', array('name' => 'metrolastsync', 'value' => time()));
				} else {
					  $lastsyncdate = time();
						$DB->execute('update {config} set value="'.$lastsyncdate.'" where name="metrolastsync"');
				}
				$info = empty($info) ? '| Nothing new insert & update records' : $info;
				mtrace('Metroc sync completed '.$info);
    }
}