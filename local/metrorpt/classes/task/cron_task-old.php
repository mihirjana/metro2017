<?php

/**
 * This script runs along with cron to sync the users from other tenants
 * and insert into mdl_user_report table.
 *
 * @package MetroSync
 * @copyright CBSI-Connect Metro
 * @date 02-aug-2016
 */
namespace local_metrorpt\task;

include_once $CFG->dirroot.'/user/profile/lib.php';


class cron_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('pluginname', 'local_metrorpt');
    }

    /**
     * Sync users information across tenant
     * 
     * @throws dml_connnection_exception
     * @return array
     */
    public function execute() {
        global $CFG, $DB;
		$tempdb = null;
		$tempdb = $DB;
        mtrace('Sync started');
        $tenants = $DB->get_records('multitenant_master', null, '', 'id, sub_domain');
		// Find user from tenants
		foreach($tenants as $tenant) {
		    $class = get_class($DB);
		    $externaldb = new $class();
		    try {
		        // Connect tenant database
				if ($tenant->sub_domain == 'a1') { $tenant->sub_domain = 'aone';}
				if ($tenant->sub_domain == 'a2') { $tenant->sub_domain = 'atwo';}
				
		        $externaldb->connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass, 'moodle_'.$tenant->sub_domain, $CFG->prefix, $CFG->dboptions);
		    } catch (dml_connection_exception $e) {
		        throw $e;
		    }
			
				
				$DB = $externaldb;
			
		    $users = $externaldb->get_records('user', array('deleted'=>0,'suspended'=>0));
            $admins = $externaldb->get_field('config', 'value', array('name' => 'siteadmins'));
            
            foreach($users as $user) {
		        profile_load_data($user);
                $admin = explode(',', $admins);
				
                if(in_array($user->id, $admin)) {
                    $user->role = 'admin';
                    //mtrace('admin '.$user->id. ' for tenant '.$tenant->sub_domain);
                }
                if(!isset($user->profile_field_gender)) {
                    $user->profile_field_gender = '';
                }
		        if(!isset($user->role)) {
                    $user->role = '';
                }
                
                $user->tenantid = $tenant->id;
		    }
		    $tenantusers[$tenant->id] = $users;
		}
		// Identify update / insert users
		$DB = $tempdb;
    	foreach($tenantusers as $tenantid => $users) {
			
		    foreach($users as $user) {
                $existsuser = $DB->get_record('user_report', ['username' => $user->username, 'tenantid' => $tenantid]);
		        // check user if exists then update else insert
		        if($existsuser) {
                    $user->id = $existsuser->id;
                    // check any diff between exitsing user to tenant user
                    $updateflag = false;
                    foreach($existsuser as $field => $value) {
						if (!isset($user->$field)){
							continue;
						}
		                if($user->$field !== $value) {
                            $updateflag = true;
                        }
                    }
                    if($updateflag) {
                        $user->id = $existsuser->id;
		                $updateusers[$user->id] = $user;
		                continue;
                    }
                    continue;
		        }
                // New user
                unset($user->id);
		        $newusers[] = $user;
            }
		}
		// insert & update all users
		$str = '';
		try {
		    if(isset($updateusers)) {
		        foreach($updateusers as $user) {
		            $DB->update_record('user_report', $user);
		        }
				$str .= count($updateusers).' updated and ';
		    }
		    
            if(isset($newusers)) {
				//var_dump($newusers);
		       foreach($newusers as $user) {
		            $DB->insert_record('user_report', $user);
               }
			   $str .= count($newusers).' newsly inserted';
            }
		} catch (dml_exception $e) {
		        throw $e;
		}
		mtrace('Metro report sync completed'.$str);
    }
}
