<?php

require_once("../../config.php");

global $CFG,$DB;

require_once($CFG->libdir . "/externallib.php");
require_once($CFG->dirroot. "/course/lib.php");

class local_metrostargetcourse_external extends external_api {

   public static function get_course_parameters() {
         return new external_function_parameters(
           array(  'category' => new external_value(PARAM_INT, 'course category id and could be subcategory id'),
                'id' => new external_value(PARAM_INT, 'course id'),
                'filter' => new external_value(PARAM_RAW, 'custom filter: acad,teach,student')
           )
          );
    }

    public static function get_course($category, $id, $filter) {
        global $CFG,$USER,$DB;
        
        if($filter=='acad'){
           $sql="select id,category,fullname,shortname,summary,startdate,visible,timecreated,requested from mdl_course where category=".$category;
        }elseif($filter=='teach'){
           $sql='SELECT c.id,c.category,c.fullname,c.shortname,c.summary,c.startdate,c.visible,c.timecreated,c.requested 
FROM mdl_user u, mdl_role_assignments r, mdl_context cx, mdl_course c
WHERE u.id = r.userid
AND r.contextid = cx.id
AND cx.instanceid = c.id
AND r.roleid =4
AND cx.contextlevel =50
AND u.id='.$id;
        }elseif($filter=='student'){
           $sql='SELECT c.id,c.category,c.fullname,c.shortname,c.summary,c.startdate,c.visible,c.timecreated,c.requested 
FROM mdl_user u, mdl_role_assignments r, mdl_context cx, mdl_course c
WHERE u.id = r.userid
AND r.contextid = cx.id
AND cx.instanceid = c.id
AND r.roleid =5
AND cx.contextlevel =50
AND u.id='.$id;
        }

        try{
        $the_courses = $DB->get_records_sql($sql);  
        return $the_courses;
        } catch(Exception $e){
        return FALSE;
        }
        
}

    /**
* Returns description of method result value
* @return external_description
*/
    public static function get_course_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array('id' => new external_value(PARAM_INT, 'course id'),
                'category' => new external_value(PARAM_INT, 'coruse category id'),
                'fullname' => new external_value(PARAM_RAW, 'course fullname'),
                'shortname' => new external_value(PARAM_RAW, 'course shortname'),
                'summary' => new external_value(PARAM_RAW, 'course summary'),
                'startdate' => new external_value(PARAM_RAW, 'start date'),
                'visible' => new external_value(PARAM_INT, 'is it visible'),
                'timecreated' => new external_value(PARAM_RAW, 'time it was created'),
                'requested' => new external_value(PARAM_INT, 'allow requests')
                )
              )
          );
    }




}