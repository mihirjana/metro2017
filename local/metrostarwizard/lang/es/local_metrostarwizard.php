<?php
$string['pluginname'] = 'Asistente';

//exists for multiple pages
$string['exit_wizard'] = 'Asistente de Salida';
$string['wizard_name'] = 'Asistente de la Creación de Cursos de CBSI-Connect';
$string['previous'] = 'Anterior';
$string['next'] = 'Próxima';


//page one
$string['page_one_question'] = '¿Qué es lo que usted desea hacer hoy?';
$string['create_new_meeting_training'] = 'Crear una reunión o capacitación nueva';



//page two
$string['page_two_question'] = '¿Qué es lo que usted desea crear?';
$string['meeting'] = 'Reunión';
$string['training'] = 'Capacitación';


//page three
$string['page_three_question'] = '¿Usted quiere que sea disponible a otras academias?';
$string['no_private_course'] = 'No:<br>Será privada';
$string['yes_all'] = 'Sí:<br> A todas las Academias';
$string['yes_selected'] = 'Sí:<br> Sólamente para las academias seleccionadas';


//page four
$stsring['course_name'] = '¿Qué es el título?';
$string['course_language'] = '¿En cuál idioma estará disponible?';
$string['course_description'] = '¿Cuál es la descripción?';
$string['course_sections'] = '¿Cuántas secciones/temas?';

//page six
$string['save_go_home'] = 'Guárdela y vuélvame a la página de inicio';
$string['delete_go_home'] = 'Bórrela y vuélvame a la página de inicio';
$string['save_start'] = 'Guárdela y inicie la configuracción';


?>











