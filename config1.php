<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'cbsidb.cay6upvsngve.us-east-1.rds.amazonaws.com';
$CFG->dbname    = 'moodle_cbsimaster';
$CFG->dbuser    = 'root';
$CFG->dbpass    = 'Hello123!';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

$CFG->wwwroot   = 'http://'.$_SERVER['SERVER_ADDR'];
$CFG->dataroot  = '/var/www/metrodatafolder32';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
