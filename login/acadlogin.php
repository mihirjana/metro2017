<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Logs the user out and sends them to the home page
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../config.php');

$PAGE->set_url('/login/acadlogin.php');
$PAGE->set_context(context_system::instance());

$sesskey = optional_param('sesskey', '__notpresent__', PARAM_RAW); // we want not null default to prevent required sesskey warning
$login   = optional_param('loginpage', 0, PARAM_BOOL);
$acad   = optional_param('acad','cdpf', PARAM_RAW);

// can be overridden by auth plugins
if ($acad) {
	$redurl = 'https://'.$acad.'.cbsi-connect.org/login/index.php';
} else {
	$redurl = $CFG->wwwroot.'/';
}

/* if ($login) {
    //$redirect = get_login_url();
	echo 'hello';
    $redirect = $redurl;
} else {
    $redirect = $CFG->wwwroot.'/';
} */
global $SITE;
$redirect = $redurl;
$goback = $CFG->wwwroot.'/';

if (!isloggedin()) {
    // no confirmation, user has already logged out
    require_logout();
    redirect($redirect);

} else if (!confirm_sesskey($sesskey)) {
    $PAGE->set_title($SITE->fullname);
    $PAGE->set_heading($SITE->fullname);
    echo $OUTPUT->header();
    // echo $OUTPUT->confirm(get_string('logoutconfirm'), new moodle_url($PAGE->url, array('sesskey'=>sesskey())), $CFG->wwwroot.'/');
	//echo "You will be logged out from ".$SITE->shortname." and will be redirected to login page of selected academy";
	//"You will be directed to your academy website. Please sign in with your username and password. Shared content, Tutorials and Global Calendar are still available on the CBSI-Connect
	
	echo "<div class='alert'>
			  <center><h5>".get_string('acad_loginmsg','theme_lambda')."</h5></center>                                                                                                               
			</div>";
    //echo $OUTPUT->confirm(get_string('logoutconfirm'), new moodle_url($PAGE->url, array('sesskey'=>sesskey(), 'acad' => $acad)), $CFG->wwwroot.'/');
    echo '<div class="acaddiv">';
    echo '<center>';
    echo '<h5>'.get_string('switchconfirm','theme_lambda').'</h5>';
	echo '<p></p>';
	echo '<a class="btn buttonacad" target = "_blank" href ='.$redirect.'>'.get_string('continue').'</a>'.'             '. '<a class="btn buttonacad" href ='.$goback.'>'.get_string('cancel').'</a>' ;
    echo '</center>';
    echo '</div>';
	echo $OUTPUT->footer();
    die;
}

/* $authsequence = get_enabled_auth_plugins(); // auths, in sequence
foreach($authsequence as $authname) {
    $authplugin = get_auth_plugin($authname);
    $authplugin->logoutpage_hook();
}

require_logout(); */

redirect($redirect);