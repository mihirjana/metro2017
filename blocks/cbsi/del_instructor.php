<?php
require_once('../../config.php');
global $CFG;
require_once($CFG->dirroot.'/blocks/cbsi/lib.php');
require_once($CFG->dirroot.'/lib/accesslib.php');
require_login();

$users    = $_POST['users'];
$courseid = $_POST['courseid'];

$failed_unenrollment = array();

$contextid = context_course::instance($courseid);


foreach($users as $user) {
    role_unassign(12, $user, $contextid->id);
}

header('Content-Type: application/json');

if(count($failed_unenrollment) === 0) {
    echo json_encode($failed_unenrollment);
} else {
    $html = "The following user IDs could not be removed as instructors for this course.\n\n";

    foreach($failed_unenrollment as $f) {
       $html .= "$f\n";
    }
    echo json_encode($html);
}
