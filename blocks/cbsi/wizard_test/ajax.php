<?php
ini_set ('display_errors', 'on');
ini_set ('log_errors', 'on');
ini_set ('display_startup_errors', 'on');
ini_set ('error_reporting', E_ALL);

require_once('../../../config.php');
global $DB,$CFG,$USER,$PAGE;
require_once($CFG->dirroot.'/blocks/cbsi/lib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot. '/enrol/locallib.php');

require_login();

$course_title = $_POST['course'][0];
$description  = $_POST['course'][1];
$category     = $_POST['course'][2];
$language     = $_POST['course'][3];
$course       = null;

//optional parameters
if(isset($_POST['course'][4])) {
    $startdate    = $_POST['course'][4];
}

if(isset($_POST['course'][5])) {
    $starttime    = $_POST['course'][5];
}

if(isset($_POST['course'][6])) {
    $enddate      = $_POST['course'][6];
}

if(isset($_POST['course'][7])) {
    $endtime      = $_POST['course'][7];
}

/**
 * generate random string of characters
 *
 * @param int
 * @return string
 */
function random($length = 10)
{
    $chars = 'bcdfghjklmnprstvwxzaeiou';

    for ($p = 0; $p < $length; $p++)
    {
        $result = ($p%2) ? $chars[mt_rand(19, 23)] : $chars[mt_rand(0, 18)];
    }

    return $result;
}

//build up course object
$data     = new stdClass();

if($category == "Meeting") {
    $data->category = get_meeting_id();
}

if($category == "Training") {
    $sql            = <<<SQL
      SELECT id FROM mdl_course_categories WHERE idnumber = '{$USER->institution}'
SQL;
    $result         = $DB->get_record_sql($sql);
    $data->category = $result->id;
}

$data->fullname   = $course_title;
$data->shortname  = random();
$data->summary    = $description;
$data->lang       = $language;

if($category == "Meeting")
{
    $data->format = "meeting"; //meeting is custom course format for cbsi
} else {
    $data->format = "topics"; //default of topics
}

$data->visible    = 1;
$data->sortorder  = 0;
$data->idnumber   = "";
$data->showgrades = 1;
$data->newsitems  = 0;
$data->startdate  = get_unix_timestamp($startdate,$starttime);
$data->marker     = 0;
$data->maxbytes   = 0;

try {
    $course = create_course($data);
} catch (Exception $e) {
    header('Content-Type: application/json');
    echo json_encode(array("error" => $e->getMessage(), "line" => __LINE__, "data" => json_encode($data)));
}

//set topics to zero for meetings
if($course->format == "meeting") {
    $sql = "update mdl_course_format_options set value = 0 where name = 'numsections' AND courseid = " . $course->id;
    $DB->execute($sql);
}

try {
    $sql = "UPDATE mdl_course SET shortname = '{$category}_{$course->id}' WHERE id = " . $course->id;
    $DB->execute($sql);
} catch(Exception $e) {
    header('Content-Type: application/json');
    echo json_encode(array("error" => $e->getMessage(), "line" => __LINE__));
}

try {
    $end_datetime = get_unix_timestamp($enddate, $endtime);
    $sql = "UPDATE mdl_block_cbsi SET end_datetime = " . $end_datetime ."  WHERE courseid = " . $course->id;
    $DB->execute($sql);
} catch (Exception $e) {
    header('Content-Type: application/json');
    echo json_encode(array("error" => $e->getMessage(), "line" => __LINE__));
}

try {
    $category_id = get_parent_id();
    $sql = "UPDATE mdl_block_cbsi SET categoryid = " . $category_id;
    $DB->execute($sql);
} catch (Exception $e) {
    header('Content-Type: application/json');
    echo json_encode(array("error" => $e->getMessage(), "line" => __LINE__));
}

try {
    //enroll course creator into course
    $context   = context_course::instance($course->id);
    $manager   = new course_enrolment_manager($PAGE, $course);
    $instances = $manager->get_enrolment_instances();
    $today     = date('U');
    $role_id   = 9; //content contributor

    //find the manual one
    foreach ($instances as $instance) {
        if ($instance->enrol == 'manual') {
            break;
        }
    }

    $plugins = $manager->get_enrolment_plugins();
    $plugin  = $plugins['manual'];
    $plugin->enrol_user($instance, $USER->id, $role_id, $today, 0);
    $plugin->enrol_user($instance, $USER->id, 12, $today, 0); // 12 = instructor role_id


    //return course id for processing on return page
    //set response type to JSON for easy jQuery parsing
    header('Content-Type: application/json');
    echo json_encode(array('course_id' => $course->id));
} catch(Exception $e) {
    header('Content-Type: application/json');
    echo json_encode(array("error" => $e->getMessage(), "line" => __LINE__));
}
