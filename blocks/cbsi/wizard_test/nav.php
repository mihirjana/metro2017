<?php

$navTop = '<div class="navWrap"><button class="exitWizard">'. get_string('exit_wizard','block_cbsi').'</button><h1>'. get_string('wizard_name','block_cbsi').'</h1><div class="nav">
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input class="wizardButtonDisabled"  id="previous" name="back" value="'. get_string('previous','block_cbsi').'" type="button" /></td>
    </tr><ul><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pageone" value="1" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pagetwo" value="2" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pagefour" value="3" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pagesix" value="4" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pageseven" value="5" type="submit" /></td>
    </tr></li></ul>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input class="wizardButton" id="next" name="next" value="'. get_string('next','block_cbsi').'" type="button" /></td>
    </tr></div><!-- end nav --></div><!-- end navwrap-->';

$navBottom = '<div id="bottomNav" class="navWrap"><button class="exitWizard">'. get_string('exit_wizard','block_cbsi').'</button><div class="nav">
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input class="wizardButtonDisabled"  id="previous" name="back" value="Previous" type="button" /></td>
    </tr><ul><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pageone" value="1" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pagetwo" value="2" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pagefour" value="3" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pagesix" value="4" type="submit" /></td>
    </tr></li><li>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input style="" name="_qf_pageone_pageseven" value="5" type="submit" /></td>
    </tr></li></ul>
    <tr>
        <td align="right" valign="top"><b></b></td>
        <td valign="top" align="left">  <input class="wizardButton" id="next" name="next" value="Next" type="button" /></td>
    </tr></div><!-- end nav --></div><!-- end navwrap-->';
