<h2>Congratulations! Your course is setup. What would you like to do now?</h2>
<div class="wizardLeft">
    <span class="or">Or</span>
    <div id="save_home" class="wizardChoice">
        <p class="">Save and Go Back to Home Page</p>
    </div><!-- end wizardChoice -->
    <div id="delete_home" class="wizardChoice">
        <p  class="">Delete and Go Back to Home Page</p>
    </div><!-- end wizardChoice -->
</div><!-- end wizardLeft -->

<div id="save_start" class="wizardRight">
    <div class="wizardChoice">
        <p  class="" >Save and Start Setup</p>
    </div><!-- end wizardChoice -->
</div><!-- end wizardright -->

<div style="clear:both"></div>
