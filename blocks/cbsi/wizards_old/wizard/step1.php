<?php

class StepOne extends HTML_QuickForm_Page
{
    function buildForm()
    {
        global $CFG;

        $this->_formBuilt = true;
        $this->addElement('html', '<div id="container">');
        $this->addElement('html', '<div class="wizard">');
        $this->addElement('html', '<div class="navWrap">');
        $this->addElement('html', '<button class="exitWizard">' . get_string("exit_wizard", "local_metrostarwizard") . '</button>');
        $this->addElement('html', '<h1>' . get_string('wizard_name', 'local_metrostarwizard') . '</h1>');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButtonDisabled",
            "disabled" => "true"
        ));

        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');

        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('button', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap-->');
        $this->addElement('html', '<h2>' . get_string('page_one_question', 'local_metrostarwizard') . '</h2>');
        $this->addElement('html', '<div class="wizardChoices oneoption">');
        $this->addElement('html', '<div class="wizardChoice">');
        $this->addElement('html', '<p onclick=\'
        document.forms["pageone"].elements["pageAB"].value="pagetwoa";document.forms["pageone"].submit();
        \'>' . get_string("create_new_meeting_training", "local_metrostarwizard") . '</p>');
        $this->addElement('html', '</div><!-- end wizardChoice -->');
        $this->addElement('html', '</div><!-- end wizardChoices -->');
        $this->addElement('hidden', 'pageAB', '', array(
            'id' => 'pageAB'
        ));
        $this->addElement('html', '<div class="navWrap bottomNav">');
        $this->addElement('html', '<div class="nav">');
        $this->addElement('button', 'back', get_string('previous', 'local_metrostarwizard'), array(
            "class" => "wizardButtonDisabled",
            "disabled" => "true"
        ));
        $this->addElement('html', '<ul>');
        $pages  = array();
        $myName = $current = $this->getAttribute('id');
        while (null !== ($current = $this->controller->getPrevName($current)))
          {
            $pages[] = $current;
          }
        $pages   = array_reverse($pages);
        $pages[] = $current = $myName;
        while (null !== ($current = $this->controller->getNextName($current)))
          {
            $pages[] = $current;
          }
        $x = 0;
        foreach ($pages as $pageName)
          {
            $x++;
            $this->addElement('html', '<li>');
            $this->addElement('submit', $this->getButtonName($pageName), $x, array(
                'style' => ''
            ));
            $this->addElement('html', '</li>');
          }
        $this->addElement('html', '</ul>');
        $this->addElement('button', 'next', get_string('next', 'local_metrostarwizard'), array(
            "class" => "wizardButton"
        ));
        $this->addElement('html', '</div><!-- end nav -->');
        $this->addElement('html', '</div><!-- end navwrap-->');
        $this->addElement('html', '</div><!-- end wizard-->');
        $this->addElement('html', '</div><!-- end container -->');
        $this->setDefaultAction('next');
    }
}
