<?php

require_once('../../config.php');
global $CFG;

require_once("$CFG->libdir/formslib.php");

class restore_form extends moodleform {

    function definition() {
        global $CFG;

        $mform =& $this->_form; // Don't forget the underscore!

        $mform->addElement('filepicker', 'userfile', get_string('file'), null,
            array('maxbytes' => $maxbytes, 'accepted_types' => '*'));
    }// Close the function
}
