<div class="wizardChoices">
    <div id="course-dates">
        <form id="class-begin" class="wizardChoice">
            <b><?php echo get_string("page_four_header", "block_cbsi");?></b>
            <b><?php echo get_string("page_four_start", "block_cbsi");?></b>
            <!-- <label for="class_begin_datetxt"><b><?php echo get_string("page_four_date_startdate", "block_cbsi");?></b></label> -->
            <input id="class_begin_datetxt" name="class_begin_datetxt" type="text" />

            <!--<label for="class_begin_timetxt"><b><?php echo get_string("page_four_date_endtime", "block_cbsi");?></b></label>-->
            <input id="class_begin_timetxt" name="class_begin_timetxt" type="text" />
            <img src="/theme/msu/pix/ClockGraphic.png" width="39" height="39" class="clock"/>
        </form>

        <br/><br/>

        <div style="clear:both"></div>
        <form id="class-end" class="wizardChoice">
            <b><?php echo get_string("page_four_end", "block_cbsi");?></b>
            <!--label for="class_end_datetxt"><b><?php echo get_string("page_four_date_enddate", "block_cbsi");?></b></label> -->
            <input id="class_end_datetxt" name="class_end_datetxt" type="text"/>

            <!--<label for="class_end_timetxt"><b><?php echo get_string("page_four_date_endtime", "block_cbsi");?></b></label> -->
            <input id="class_end_timetxt" name="class_end_timetxt" type="text" />
            <img src="/theme/msu/pix/ClockGraphic.png" width="39" height="39" class="clock" />
        </form>
    </div><!-- end course dates -->
</div>
